import { sum } from '../../modules/sum'

describe('sum', () => {
  test('ok', () => {
    const data = [1, 2]
    const result = sum(data[0], data[1])
    const expected = data[0] + data[1]
    expect(result).toBe(expected)
  })
})
