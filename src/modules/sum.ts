/**
 * 足し算を行う関数
 * @param a
 * @param b
 */
export const sum = (a: number, b: number) => a + b
