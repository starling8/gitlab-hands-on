/**
 * 引き算を行う関数
 * @param a
 * @param b
 */
export const subtract = (a: number, b: number) => a - b
